package com.user.userms.repositories;

import com.user.userms.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    // Other custom queries if needed
}
